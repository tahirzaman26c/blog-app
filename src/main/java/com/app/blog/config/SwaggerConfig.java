package com.app.blog.config;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//import io.swagger.annotations.AuthorizationScope;
//import io.swagger.v3.oas.models.security.Scopes;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SwaggerConfig {
	
	public static final String AUTHORIZATION_HEADER = "Authorization";
	
	
	// Docker me SecuritySchemes ke liye ye key hai
	private ApiKey apiKey() { 
		return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
	}
	
	
	//Docker me securityContext ke liye
	//The SecurityContext is used to store the details of the currently authenticated user
	private List<SecurityContext> securityContexts(){
		return Arrays.asList(SecurityContext.builder().securityReferences(securityRef()).build()); //securityReference method required tha to uska niche ek alag List create kiya
	}
	private List<SecurityReference> securityRef(){
		
		AuthorizationScope scope = new AuthorizationScope("global", "accessEverything");
		return Arrays.asList(new SecurityReference("JWT", new AuthorizationScope[] {scope}));
	}
	
	
	// Swagger ki jitni bhi configuration karna hota hai wo sab ek bean Docker me hota hai
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(getInfo())
				.securityContexts(securityContexts()) // Isska methods alag se bana na hai so that it can returns list of securityContext	
				.securitySchemes(Arrays.asList(apiKey()))
				.select()
				.apis(RequestHandlerSelectors.any())
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo getInfo() {
		//title
		//description
		//version
		//TermsOfServiceUrls
		//ContactName
		//License
		//License Url
		
		return new ApiInfo("Blogging Application | Java Backend",
				"This project is developed by learn Code with Durgesh", 
				"1.0", "Terms Of Service",
				new Contact("Tahir Shaikh", "None", "tahirzama.q@gmail.com"),
				"License of API",
				"API License Url",
				Collections.emptyList());
	}
	
}
