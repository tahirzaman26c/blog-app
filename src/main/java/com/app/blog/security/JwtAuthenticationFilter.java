package com.app.blog.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	// Ye authentication filter Har Api se pehle chalega aur Validation check karega 

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private JwtTokenHelper jwtTokenHelper;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		
		
		
		// Get token (1)
		String requestToken = request.getHeader("Authorization");

		// Bearer 53215cbshjcbsaj

		System.out.println(requestToken);

		String username = null;
		String token = null;

		if (requestToken != null && requestToken.startsWith("Bearer")) {  //check karenge null toh nai hai ya bearer se strt horaha hai ki nai (2)
			token = requestToken.substring(7); //Token nikaal lenge, 0-7 tak skip karenge bearer skip hoga seedha pure code aayega (3)
			try {
				username = this.jwtTokenHelper.getUsernameFromToken(token);  // Username nikal lenge Helper se (4)
			} catch (IllegalArgumentException e) {
				System.out.println("Unable to get jwt token");
			}catch (ExpiredJwtException e) {
				System.out.println("Jwt token has expired");
			}catch (MalformedJwtException e) {
				System.out.println("Invalid JWT");
			}

		} else {
			System.out.println("jwt token does not starts with Bearer");
		}
		// Once we get the token, Now Validate (5)
		
		if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null) {    //if username null nai aur get auth khali hai (6)
			
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);  //userDetail Service se username nikaalenge (7)
			if(this.jwtTokenHelper.validateToken(token, userDetails)) {   //aur validate karenge Token aur userDetail ko (8)
				//sab sahi chalraha hai
				//authentication karna hai (9)
				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails,null, userDetails.getAuthorities());
				usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);   //Authentication ke baad setAuth se auth set kardenge
			}else {
				System.out.println("Invalid jwt token");
			}
		}else {
			System.out.println("username is null or context is not null ");
		}
		
		filterChain.doFilter(request, response);  //At last ye final chalega
		

	}

}
