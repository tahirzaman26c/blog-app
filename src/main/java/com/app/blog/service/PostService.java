package com.app.blog.service;

import java.util.List;

import com.app.blog.entities.Post;
import com.app.blog.payloads.PostDto;
import com.app.blog.payloads.PostResponse;

public interface PostService {
	
	//Create
	PostDto createPost(PostDto postDto,Integer userId,Integer categoryId, Integer postId);
	
	//update 
	PostDto updatePost(PostDto postDto ,Integer postId,Integer adminId); 
	
	//delete
	void deletePost(Integer postId);
	
	//Get All post
	PostResponse getAllPost(Integer pageNumber,Integer pageSize,String sortBy);
	
	//Get 
	PostDto getPostById(Integer postId);
	
	//get All posts By category
	List<PostDto> getAllPostsByCategory(Integer postId);
	
	//Get all posts By User
	List<PostDto> getAllPostsByUser(Integer postId);
	
	//Search Post
	List<PostDto> searchPosts(String keyword);



}
