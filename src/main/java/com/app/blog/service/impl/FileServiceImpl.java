package com.app.blog.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.app.blog.service.FileService;

@Service
public class FileServiceImpl implements FileService {

	@Override
	public String uploadImage(String path, MultipartFile file) throws IOException {
		//fileName
		String name = file.getOriginalFilename();
		//abc.png
		
		
		//random name generator file
		String randomID = UUID.randomUUID().toString();
		String fileName1 = randomID.concat(name.substring(name.lastIndexOf(".")));
		
		//Full path
		String filepath= path+File.separator+fileName1;
		
		//Create Folder if not created
		File f = new File(path);
		if(!f.exists()) {
			f.mkdir();
		}
		
		
		// File copy
		Files.copy(file.getInputStream(), Paths.get(filepath));
		
		return name;
	}

	@Override
	public InputStream getResource(String path, String fileName) throws FileNotFoundException {
		String fullpath = path+File.separator+fileName;
		InputStream is = new FileInputStream(fullpath);
		//DB logic to return inputStream
		return is;
	}

}
