package com.app.blog.service.impl;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.blog.entities.Comment;
import com.app.blog.entities.Post;
import com.app.blog.exceptions.ResourseNotFoundException;
import com.app.blog.payloads.CommentDto;
import com.app.blog.repositories.CommentRepo;
import com.app.blog.repositories.PostRepo;
import com.app.blog.service.CommentService;



@Service
public class CommentServiceImpl implements CommentService {
	
	@Autowired
	private CommentRepo commentRepo;
	
	private PostRepo postRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public CommentDto createComment(CommentDto commentDto,Integer postId) {
		Post posts = this.postRepo.findById(postId).orElseThrow(()-> new ResourseNotFoundException("post", "postId", postId));
		// TODO Auto-generated method stub
		var comm = this.modelMapper.map(commentDto, Comment.class);
		comm.setPost(posts);
		var addedComment = this.commentRepo.save(comm);
		
		return this.modelMapper.map(addedComment, CommentDto.class);
	}

	@Override
	public void deleteComment(Integer comment_Id) {
		// TODO Auto-generated method stub
		var deleteComm = this.commentRepo.findById(comment_Id).orElseThrow(
				()-> new ResourseNotFoundException("Comment", "comment_Id", comment_Id));
		this.commentRepo.delete(deleteComm);

	}

}
