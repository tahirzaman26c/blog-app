package com.app.blog.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.blog.entities.Category;
import com.app.blog.exceptions.ResourseNotFoundException;
import com.app.blog.payloads.ApiResponse;
import com.app.blog.payloads.CategoryDto;
import com.app.blog.payloads.CategoryResponse;
import com.app.blog.payloads.PostDto;
import com.app.blog.payloads.PostResponse;
import com.app.blog.repositories.CategoryRepo;
//import com.app.blog.repositories.CategoryRepo;
import com.app.blog.service.CategoryService;
@Service
public class CategoryServiceImpl implements CategoryService {
	
	@Autowired
	private CategoryRepo categoryRepo;
	
	@Autowired
	private ModelMapper modelMapper;

	@Override
	public CategoryDto createCategory(CategoryDto categoryDto) {
		// TODO Auto-generated method stub
		var cat = this.modelMapper.map(categoryDto, Category.class);
		var addedCat = this.categoryRepo.save(cat);
		return this.modelMapper.map(addedCat, CategoryDto.class);
	}

	@Override
	public CategoryDto updateCategory(CategoryDto categoryDto, Integer categoryId) {
		// TODO Auto-generated method stub
		var category = this.categoryRepo.findById(categoryId).orElseThrow(
				() -> new ResourseNotFoundException("Category", "Category_id", categoryId));
		
		category.setCategoryTitle(categoryDto.getCategoryTitle());
		category.setCategoryDescription(categoryDto.getCategoryDescription());
		
		var updatedCat = this.categoryRepo.save(category);
		return this.modelMapper.map(updatedCat, CategoryDto.class);
	}

	@Override
	public void deleteCategory(Integer categoryId) {
		// TODO Auto-generated method stub
		var deleteCat = this.categoryRepo.findById(categoryId).orElseThrow(
				()-> new ResourseNotFoundException("Category", "category_ID", categoryId));
		this.categoryRepo.delete(deleteCat);
		
	}

	@Override
	public CategoryDto getCategory(Integer categoryId) {
		// TODO Auto-generated method stub
		var getCat = this.categoryRepo.findById(categoryId).orElseThrow(
				()-> new ResourseNotFoundException("Category", "category_ID", categoryId));
		return this.modelMapper.map(getCat, CategoryDto.class);
	}

	@Override
	public CategoryResponse getCategories(Integer pageNumber,Integer pageSize) {
		Pageable p = PageRequest.of(pageNumber, pageSize);
		// TODO Auto-generated method stub
		Page<Category> pagePost = this.categoryRepo.findAll(p);
		List<Category> categories = pagePost.getContent();
		List<CategoryDto> catDtos =  categories.stream().map((category)-> this.modelMapper.map(category, CategoryDto.class)).collect(Collectors.toList());
		
		var categoryResponse = new CategoryResponse();
		categoryResponse.setContent(catDtos);
		categoryResponse.setPageNumber(pagePost.getNumber());
		categoryResponse.setPageSize(pagePost.getSize());
		categoryResponse.setTotalPages(pagePost.getTotalPages());
		categoryResponse.setTotalElement(pagePost.getTotalElements());
		
		categoryResponse.setLastPage(pagePost.isLast());
		return categoryResponse;
	}
	
	

}
