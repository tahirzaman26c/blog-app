package com.app.blog.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.app.blog.config.AppConstants;
import com.app.blog.entities.Role;
import com.app.blog.entities.User;
import com.app.blog.exceptions.ResourseNotFoundException;
import com.app.blog.payloads.UserDto;
import com.app.blog.repositories.RoleRepo;
import com.app.blog.repositories.UserRepo;
import com.app.blog.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private RoleRepo roleRepo;

	@Override
	public UserDto createUser(UserDto userDto, Integer userId) {
		// TODO Auto-generated method stub
		User user =  this.modelMapper.map(userDto, User.class);
//		User user = this.dtoToUser(userDto);
		User creatingUser = new User();
		creatingUser.setId(userId);
		user.setCreatedBy(creatingUser);
		user.setUpdatedBy(creatingUser);
		User savedUser = this.userRepo.save(user);
		
//		savedUser.setUpdatedBy(adminUser);
		return this.modelMapper.map(savedUser, UserDto.class);
//		return this.userToDto(savedUser);
	}

	@Override
	public UserDto updateUser(UserDto userDto, Integer userId,Integer adminId) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(userId).orElseThrow(
				()-> new ResourseNotFoundException("User", "id", userId) );
		
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
		user.setAbout(userDto.getAbout());
		User adminUser = new User();
		adminUser.setId(adminId);
		user.setUpdatedBy(adminUser);
		
		
		User updatedUser = this.userRepo.save(user);
		UserDto userDto1 = this.userToDto(updatedUser);
		
		return userDto1;
	}

	@Override
	public UserDto getUserById(Integer userId) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(userId).orElseThrow(
				()-> new ResourseNotFoundException("User", "id", userId) );
		return this.userToDto(user);
	}

	@Override
	public List<UserDto> getAllUsers() {
		// TODO Auto-generated method stub
		List<User> users = this.userRepo.findAll();
		List<UserDto> userDtos=  users.stream().map(user->this.userToDto(user)).collect(Collectors.toList());	
		return userDtos;
	}

	@Override
	public void deleteUser(Integer userId) {
		// TODO Auto-generated method stub
		User user = this.userRepo.findById(userId).orElseThrow(
				()-> new ResourseNotFoundException("User", "id", userId) );
		user.setIsActive(false);
		userRepo.save(user);
//		this.userRepo.delete(user);

	}
	
	private User dtoToUser(UserDto userDto) {
		User user = this.modelMapper.map(userDto,User.class);
//		user.setId(userDto.getId());
//		user.setName(userDto.getName());
//		user.setEmail(userDto.getEmail());
//		user.setAbout(userDto.getAbout());
//		user.setPassword(userDto.getPassword());
		return user;
	}
	
	private UserDto userToDto(User user) {
		UserDto userDto = this.modelMapper.map(user, UserDto.class);
//		userDto.setId(user.getId());
//		userDto.setName(user.getName());
//		userDto.setEmail(user.getEmail());
//		userDto.setAbout(user.getAbout());
//		userDto.setPassword(user.getPassword());
		return userDto;
	}

	@Override
	public UserDto registerNewUser(UserDto userDto) {
		User user = this.modelMapper.map(userDto, User.class);
		
		//encoded the password
		user.setPassword(this.passwordEncoder.encode(user.getPassword()));
		
		//roles
//		Optional<Role> findById = this.roleRepo.findById(AppConstants.NORMAL_ROLE);
		Role role = this.roleRepo.findById(AppConstants.NORMAL_ROLE).get();
		user.getRoles().add(role);
		User newUser = this.userRepo.save(user);
		
		
		return this.modelMapper.map(newUser, UserDto.class);
		
	}

}
