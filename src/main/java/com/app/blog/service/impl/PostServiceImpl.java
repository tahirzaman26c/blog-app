package com.app.blog.service.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.app.blog.entities.Category;
import com.app.blog.entities.Post;
import com.app.blog.entities.User;
import com.app.blog.exceptions.ResourseNotFoundException;
import com.app.blog.payloads.PostDto;
import com.app.blog.payloads.PostResponse;
import com.app.blog.repositories.CategoryRepo;
import com.app.blog.repositories.PostRepo;
import com.app.blog.repositories.UserRepo;
import com.app.blog.service.PostService;

@Service
public class PostServiceImpl implements PostService {
	
	@Autowired
	private PostRepo postRepo;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private CategoryRepo categoryRepo;

	@Override
	public PostDto createPost(PostDto postDto,Integer userId,Integer categoryId ,Integer postId) {
		
		User user = this.userRepo.findById(userId).orElseThrow(()-> new ResourseNotFoundException("User", "id", userId));
		
		Category category = this.categoryRepo.findById(categoryId).orElseThrow(()-> new ResourseNotFoundException("Category", "id", categoryId));
		
		// TODO Auto-generated method stub
		var post = this.modelMapper.map(postDto, Post.class);
		User creatingUser = new User();
		creatingUser.setId(postId);
		post.setCreatedBy(creatingUser);
		post.setUpdatedBy(creatingUser);
		post.setImageName("default.pmg");
		post.setAddedDate(new Date());
		post.setUser(user);
		post.setCategory(category);
		
		var created = this.postRepo.save(post);
		
		return this.modelMapper.map(created, PostDto.class);
	}

	@Override
	public PostDto updatePost(PostDto postDto, Integer postId,Integer adminId) {
		// TODO Auto-generated method stub
		var updatePost = this.postRepo.findById(postId).orElseThrow(
				() -> new ResourseNotFoundException("Post", "Post_id", postId));
		
		updatePost.setTitle(postDto.getTitle());
		updatePost.setContent(postDto.getContent());
		updatePost.setImageName(postDto.getImageName());
//		updatePost.setAddedDate(postDto.getAddedDate());
		User adminUser = new User();
		adminUser.setId(adminId);
		updatePost.setUpdatedBy(adminUser);

		var updated = this.postRepo.save(updatePost);
		return this.modelMapper.map(updated, PostDto.class);
	}

	@Override
	public void deletePost(Integer postId) {
		// TODO Auto-generated method stub
		var postDelete = this.postRepo.findById(postId).orElseThrow(
				() -> new ResourseNotFoundException("Post", "postId", postId));
		this.postRepo.delete(postDelete);

	}

	@Override
	public PostResponse getAllPost(Integer pageNumber,Integer pageSize,String sortBy) {
		//Pagination
		Pageable p = PageRequest.of(pageNumber, pageSize,Sort.by(sortBy));
		
		Page<Post> pagePost =  this.postRepo.findAll(p);
		
		List<Post> posts = pagePost.getContent(); 
		List<PostDto> postDtos = posts.stream().map((post)-> this.modelMapper.map(post, PostDto.class)).collect(Collectors.toList());
		// TODO Auto-generated method stub
		
		var postRespose = new PostResponse();
		postRespose.setContent(postDtos);
		postRespose.setPageNumber(pagePost.getNumber());
		postRespose.setPageSize(pagePost.getSize());
		postRespose.setTotalPages(pagePost.getTotalPages());
		postRespose.setTotalElement(pagePost.getTotalElements());
		
		postRespose.setLastPage(pagePost.isLast());
		
		
		return postRespose;
	}

	@Override
	public PostDto getPostById(Integer postId) {
		var getPost = this.postRepo.findById(postId).orElseThrow(
				()-> new ResourseNotFoundException("Post", "postId", postId));
		// TODO Auto-generated method stub
		return this.modelMapper.map(getPost, PostDto.class);
	}

	@Override
	public List<PostDto> getAllPostsByCategory(Integer categoryId) {
		// TODO Auto-generated method stub
		var getAllPostsByCat = this.categoryRepo.findById(categoryId).orElseThrow(
				()-> new ResourseNotFoundException("Category", "category_ID", categoryId));
		List<Post> posts = this.postRepo.findByCategory(getAllPostsByCat);
		List<PostDto> postDtos = posts.stream().map((post)-> this.modelMapper.map(post, PostDto.class)).collect(Collectors.toList());
		return postDtos;
	}

	@Override
	public List<PostDto> getAllPostsByUser(Integer userId) {
		// TODO Auto-generated method stub
		var getAllPostByUser = this.userRepo.findById(userId).orElseThrow(()-> new ResourseNotFoundException("User", "userId", userId));
		
		List<Post> posts = this.postRepo.findByUser(getAllPostByUser);
		List<PostDto> postDtos = posts.stream().map((post)-> this.modelMapper.map(post, PostDto.class)).collect(Collectors.toList()); 
		return postDtos;
	}

	@Override
	public List<PostDto> searchPosts(String keyword) {
		// TODO Auto-generated method stub
		List<Post> posts = this.postRepo.findByTitleContainingIgnoreCaseAndContentContainingIgnoreCase(keyword,keyword);
		List<PostDto> postDto = posts.stream().map((post)->this.modelMapper.map(post, PostDto.class)).collect(Collectors.toList());
		return postDto;
	}

}
