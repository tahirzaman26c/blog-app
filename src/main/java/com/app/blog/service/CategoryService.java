package com.app.blog.service;

//import java.util.List;

//import com.app.blog.payloads.ApiResponse;
import com.app.blog.payloads.CategoryDto;
import com.app.blog.payloads.CategoryResponse;
//import com.app.blog.payloads.UserDto;

public interface CategoryService {
	//Create
	CategoryDto createCategory(CategoryDto categoryDto);
	//Update
	CategoryDto updateCategory(CategoryDto categoryDto,Integer categoryId);
	//delete
	void deleteCategory(Integer categoryId);
	//Get
	CategoryDto getCategory(Integer categoryId);
	//Get All
	CategoryResponse getCategories(Integer pageNumber,Integer pageSize);

}
