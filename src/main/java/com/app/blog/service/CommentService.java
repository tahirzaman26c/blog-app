package com.app.blog.service;

import com.app.blog.payloads.CommentDto;

public interface CommentService {
	// Create Comments
	CommentDto createComment(CommentDto commentDto,Integer postId ); 
	
	// Delete Commments
	void deleteComment(Integer comment_Id);
}
