package com.app.blog.service;

import java.util.List;

import com.app.blog.payloads.UserDto;

public interface UserService {
	UserDto registerNewUser(UserDto userDto);
	UserDto createUser(UserDto user,Integer userId);
	UserDto updateUser(UserDto user, Integer userId, Integer adminId);
	UserDto getUserById(Integer userId);
	List<UserDto> getAllUsers();
	void deleteUser(Integer userId);

}
