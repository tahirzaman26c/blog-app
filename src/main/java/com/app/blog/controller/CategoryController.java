package com.app.blog.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.blog.payloads.ApiResponse;
import com.app.blog.payloads.CategoryDto;
import com.app.blog.payloads.CategoryResponse;
import com.app.blog.service.CategoryService;

@RestController
@RequestMapping("/api/category")
public class CategoryController {
	@Autowired
	private CategoryService categoryService;
	
	//POST - Create User
	@PostMapping
	public ResponseEntity<CategoryDto> createCategory(@Valid @RequestBody CategoryDto categoryDto){
		var createCategoryDto = this.categoryService.createCategory(categoryDto);
		return new ResponseEntity<CategoryDto>(createCategoryDto, HttpStatus.CREATED);
	}
	
	//PUT - update User
	@PutMapping("{categoryId}")
	public ResponseEntity<CategoryDto> updateCategory(@Valid @RequestBody CategoryDto categoryDto,
			@PathVariable Integer categoryId){
		var updateCategoryDto = this.categoryService.updateCategory(categoryDto, categoryId);
		return new ResponseEntity<CategoryDto>(updateCategoryDto, HttpStatus.OK);
	}
	//Delete - delete user
	@DeleteMapping("{categoryId}")
	public ResponseEntity<ApiResponse> deleteCategory(@PathVariable Integer categoryId){
		this.categoryService.deleteCategory( categoryId);
		return new ResponseEntity<ApiResponse>(new ApiResponse("category is deleted ",true), HttpStatus.OK);
	}
	//Get -  get User
	@GetMapping("{categoryId}")
	public ResponseEntity<CategoryDto> getCategory(@PathVariable Integer categoryId){
		var getCategoryDto = this.categoryService.getCategory(categoryId);
		return new ResponseEntity<CategoryDto>(getCategoryDto, HttpStatus.OK);
	}
	// Get All - get all users
   @GetMapping
   public ResponseEntity<CategoryResponse> getCategories(
		   @RequestParam(value = "pageNumber",defaultValue = "0",required = false) Integer pageNumber,
		   @RequestParam(value = "pageSize",defaultValue = "10",required = false) Integer pageSize){
		CategoryResponse categoryResponse = this.categoryService.getCategories(pageNumber,pageSize);
		return new ResponseEntity<CategoryResponse>(categoryResponse, HttpStatus.OK);
	}
//	@GetMapping()
//	public ResponseEntity<List<CategoryDto>> getCategories(){
//		var getCategoriesDto = this.categoryService.getCategories();
//		return new Res

	

}
