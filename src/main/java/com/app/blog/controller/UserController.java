package com.app.blog.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.blog.payloads.ApiResponse;
import com.app.blog.payloads.UserDto;
import com.app.blog.service.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	
	//inject Service Dependencies
	//it automatically inject dependencies when its needed
	@Autowired
	private UserService userService;
	
	
	//POST - Create User
	@PostMapping
	public ResponseEntity<UserDto> createUser(@Valid @RequestBody UserDto userDto,@PathVariable Integer userId) {
		UserDto createUserDto = this.userService.createUser(userDto,userId);
		return new ResponseEntity<UserDto>(createUserDto,HttpStatus.CREATED);
	}
	
	//PUT - update User
	@PutMapping("{userId}")
	public ResponseEntity<UserDto> updateUser(@Valid @RequestBody UserDto userDto,@PathVariable Integer userId,@PathVariable Integer adminId){
		var updateUserDto = this.userService.updateUser(userDto, userId,adminId);
		return new ResponseEntity<UserDto>(updateUserDto, HttpStatus.OK);
	}

	//GET - Get user
	@GetMapping("{userId}")
	public ResponseEntity<UserDto> getUserById(@PathVariable Integer userId){
		var getUserDto = this.userService.getUserById(userId);
		return new ResponseEntity<UserDto>(getUserDto, HttpStatus.OK);
	}
	
	//ADMIN
	//DELETE - Delete user
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@DeleteMapping("{userId}")
	public ResponseEntity<ApiResponse> deleteUser(@PathVariable Integer userId){
		this.userService.deleteUser(userId);
		return new ResponseEntity<ApiResponse>(new ApiResponse("User Deleted Successfully xD",true) ,HttpStatus.OK);
		
	}
//	--------------------------OR--------------------------
//	public ResponseEntity<String> deleteUser(@PathVariable Integer userId){
//		this.userService.deleteUser(userId);
//		return new ResponseEntity<String>("User Deleted Successfully xD", HttpStatus.OK);
//		
//	}
	
	
	
	
	
	
	

}
