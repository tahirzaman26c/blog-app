package com.app.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.blog.payloads.ApiResponse;
import com.app.blog.payloads.CommentDto;
import com.app.blog.service.CommentService;

@RestController
@RequestMapping("/api/")
public class CommentController {
	
	@Autowired
	private CommentService commentService;
	
	// Create Comments
	@PostMapping("/post/{postId}/comments")
	public ResponseEntity<CommentDto> createComment(@RequestBody CommentDto commentDto,@PathVariable Integer postId){
		var created = this.commentService.createComment(commentDto,postId);
		return new ResponseEntity<CommentDto>(created, HttpStatus.CREATED);
	}
	
	// Delete Comments
	@DeleteMapping("/comments/{comment_Id}")
	public ResponseEntity<ApiResponse> deleteComment(@PathVariable Integer comment_Id){
		this.commentService.deleteComment(comment_Id);
		return new ResponseEntity<ApiResponse>(new ApiResponse("Deleted Successfully xD", true),HttpStatus.OK);
	}


}
