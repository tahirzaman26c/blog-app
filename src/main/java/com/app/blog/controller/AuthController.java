package com.app.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.blog.exceptions.ApiException;
import com.app.blog.payloads.JwtAuthRequest;
import com.app.blog.payloads.JwtAuthResponse;
import com.app.blog.payloads.UserDto;
import com.app.blog.security.JwtTokenHelper;
import com.app.blog.service.UserService;

@RestController
@RequestMapping("/api/v1/auth/")
public class AuthController {
	
	@Autowired
	private JwtTokenHelper jwtTokenHelper;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping("/login")
	public ResponseEntity<JwtAuthResponse> createToken(@RequestBody JwtAuthRequest request) throws Exception{  // Request aaygi login pe aur jo Username pass aayga wo iss request pe aajayga
		
		
		this.authenticate(request.getUsername(),request.getPassword());   //--(1)-- Sab se pehle Authenticate karenge uske liye authenticate ka method banayenge
		
		
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(request.getUsername());//--(6)-- Yaha se User ki details nikaal lenge 
		String token = this.jwtTokenHelper.generateToken(userDetails);//--(5)-- Agar sab kuch sahi hai yahan tak toh Token generate karna hoga but uske liye hame user ki details chaye hogi
		//Ab is generated token ko hame bhejna hai toh hum isska variable banayenge aur return kar denge
		
		
		var response = new JwtAuthResponse();
		response.setToken(token);
		return new ResponseEntity<JwtAuthResponse>(response, HttpStatus.OK);
		
	}


	private void authenticate(String username, String password) throws Exception {   //--(2)-- ye method hai authenticate ka
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password); //--(4)--toh yahan se hum username pass nikalenge authenticate ke liye
		try {
			this.authenticationManager.authenticate(authenticationToken);  //--(3)-- issme authenticateManager ke paas authenticate karne ka method hai but issme username password required hai authenticate karne ke liye

		} catch (BadCredentialsException e) {
			System.out.println("Invalid Details xD ");
			throw new ApiException("Invalid username and password xD");
			
		}
	}
	
	//Register New user api
		@PostMapping("/register")
		public ResponseEntity<UserDto> registeredUser(@RequestBody UserDto userDto){
			UserDto registeredUser = this.userService.registerNewUser(userDto);
			
			return new ResponseEntity<UserDto>(registeredUser, HttpStatus.CREATED);
		}


}

























