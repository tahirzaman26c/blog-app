package com.app.blog.payloads;

import com.app.blog.entities.Post;
import com.app.blog.entities.User;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentDto {
	private int comment_Id;
	
	private String content;
	
//	private Post post;
//	
//	private User user;
}
