package com.app.blog.payloads;

import lombok.Data;

@Data
public class RoleDto {
	private int Id;
	private String name;

}
